#!/bin/bash

# SPDX-FileCopyrightText: 2022 Thomas Kramer
#
# SPDX-License-Identifier: CC0-1.0

# Import the core code of coloquinte into this repository.

set -e

SRC_REPO=https://codeberg.org/tok/Coloquinte_PlaceRoute

DEST_DIR=$(mktemp -d)

function clean () {
    rm -rf $DEST_DIR
}

trap clean EXIT

git -C $DEST_DIR clone $SRC_REPO coloquinte


SRC_DIR=src/cpp/coloquinte
mkdir -p $SRC_DIR


cp -rv $DEST_DIR/coloquinte/src $SRC_DIR
cp -rv $DEST_DIR/coloquinte/test $SRC_DIR
cp -rv $DEST_DIR/coloquinte/thirdparty $SRC_DIR
cp -v $DEST_DIR/coloquinte/README* $SRC_DIR
cp -v $DEST_DIR/coloquinte/LIC* $SRC_DIR
cp -v $DEST_DIR/coloquinte/CMakeLists.txt $SRC_DIR

echo "done"

<!--
SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Coloquinte Placement for LibrEDA

This crate is a Rust wrapper around the [Coloquinte placement algorithm](https://github.com/Coloquinte/PlaceRoute).

# Building

The placement algorithm is written in C++ and comes with a C API which is used from the Rust crate.
The C++ code is directly compiled by the `build.rs` script but a C++ compiler must be available.

The C++ code is included in this repository. The `fetch_coloquinte.sh` script helps to update the C++ code from git.
C++ code is directoy included (instead of using submodules) to not disrupt the build process with downloads.

# Test

The crate contains some simple unit tests. They can be run with `cargo test`.

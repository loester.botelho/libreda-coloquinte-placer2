// SPDX-FileCopyrightText: 2022 Thomas Kramer
//
// SPDX-License-Identifier: CC0-1.0

use cmake;

fn main() {

    let dst = cmake::Config::new("src/cpp/coloquinte")
        .build_target("coloquinte_static")
        .define("BUILD_SHARED_LIBS", "OFF")
        .build();

    // Tell cargo where to find the static library ('libcoloquinte_static.a' on Linux systems).
    println!("cargo:rustc-link-search=native={}/build", dst.display());

    // Tell cargo the library name.
    println!("cargo:rustc-link-lib=static=coloquinte_static");

    // Set C++ standard library.
    let cxx_stdlib = match std::env::var("CXXSTDLIB") {
        Ok(s) if s.is_empty() => None,
        Ok(s) => Some(s),
        Err(_) => {
            let target = std::env::var("TARGET").unwrap();
            if target.contains("freebsd") {
                Some("c++".to_string())
            } else if target.contains("openbsd") {
                Some("c++".to_string())
            } else {
                Some("stdc++".to_string())
            }
        }
    };

    if let Some(cxx) = cxx_stdlib {
        // Tell cargo to link to the c++ standard library.
        println!("cargo:rustc-link-lib={}", cxx);
    }
}
